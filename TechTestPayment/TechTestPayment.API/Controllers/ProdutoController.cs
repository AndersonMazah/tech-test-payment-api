using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Interfaces;
using TechTestPayment.Dominio.Exceptions;
using TechTestPayment.Dominio.Models;
using TechTestPayment.Dominio.Utils;

namespace TechTestPayment.API.Controllers;

[Route("api/produto")]
[ApiController]
public class ProdutoController : ControllerBase
{
    private readonly IProdutoAplicacao _produtoAplicacao;

    public ProdutoController(IProdutoAplicacao ProdutoAplicacao)
    {
        _produtoAplicacao = ProdutoAplicacao;
    }

    [HttpGet]
    public async Task<ActionResult<List<ProdutoDto>>> BuscarTodosProdutos()
    {
        try
        {
            List<ProdutoDto> retorno = await _produtoAplicacao.BuscarTodosAsync();
            return StatusCode(StatusCodes.Status200OK, retorno);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }

    [HttpGet("{guid:Guid}")]
    public async Task<ActionResult<ProdutoDto>> BuscarProdutoPorGuid(Guid guid)
    {
        try
        {
            ProdutoDto retorno = await _produtoAplicacao.BuscarPorGuidAsync(guid);
            return StatusCode(StatusCodes.Status200OK, retorno);
        }
        catch (RegistroNaoEncontradoException e)
        {
            return StatusCode(StatusCodes.Status204NoContent, e.Message);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }

    [HttpPost]
    public async Task<ActionResult<ProdutoDto>> CadastrarProduto(CadastroProdutoModel modelo)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(StatusCodes.Status409Conflict, Messages.MessagePreencherCampos);
            }
            ProdutoDto retorno = await _produtoAplicacao.CadastrarAsync(modelo);
            return StatusCode(StatusCodes.Status201Created, retorno);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }

    [HttpPut]
    public async Task<ActionResult<ProdutoDto>> AtualizarProduto(AtualizaProdutoModel modelo)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(StatusCodes.Status409Conflict, Messages.MessagePreencherCampos);
            }
            ProdutoDto retorno = await _produtoAplicacao.AtualizarAsync(modelo);
            return StatusCode(StatusCodes.Status200OK, retorno);
        }
        catch (RegistroNaoEncontradoException e)
        {
            return StatusCode(StatusCodes.Status204NoContent, e.Message);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }

    [HttpDelete("{guid:Guid}")]
    public async Task<ActionResult> DeletarProduto(Guid guid)
    {
        try
        {
            await _produtoAplicacao.DeletarAsync(guid);
            return StatusCode(StatusCodes.Status200OK, Messages.MessageExcluidoComSucesso);
        }
        catch (RegistroNaoEncontradoException e)
        {
            return StatusCode(StatusCodes.Status204NoContent, e.Message);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }
}
