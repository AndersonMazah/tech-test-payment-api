using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Interfaces;
using TechTestPayment.Dominio.Exceptions;
using TechTestPayment.Dominio.Models;
using TechTestPayment.Dominio.Utils;

namespace TechTestPayment.API.Controllers;

[Route("api/venda")]
[ApiController]
public class VendaController : ControllerBase
{
    private readonly IVendaAplicacao _vendaAplicacao;

    public VendaController(IVendaAplicacao vendaAplicacao)
    {
        _vendaAplicacao = vendaAplicacao;
    }

    [HttpGet]
    public async Task<ActionResult<List<VendaDto>>> BuscarTodosVendaes()
    {
        try
        {
            List<VendaDto> retorno = await _vendaAplicacao.BuscarTodosAsync();
            return StatusCode(StatusCodes.Status200OK, retorno);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }

    [HttpGet("{guid:Guid}")]
    public async Task<ActionResult<VendaDto>> BuscarVendaPorGuid(Guid guid)
    {
        try
        {
            VendaDto retorno = await _vendaAplicacao.BuscarPorGuidAsync(guid);
            return StatusCode(StatusCodes.Status200OK, retorno);
        }
        catch (RegistroNaoEncontradoException e)
        {
            return StatusCode(StatusCodes.Status204NoContent, e.Message);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }

    [HttpPost]
    public async Task<ActionResult<VendaDto>> CadastrarVenda(CadastroVendaModel modelo)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(StatusCodes.Status409Conflict, Messages.MessagePreencherCampos);
            }
            VendaDto retorno = await _vendaAplicacao.CadastrarAsync(modelo);
            return StatusCode(StatusCodes.Status201Created, retorno);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }

    [HttpPut]
    public async Task<ActionResult<VendaDto>> AtualizarVenda(AtualizaVendaModel modelo)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(StatusCodes.Status409Conflict, Messages.MessagePreencherCampos);
            }
            VendaDto retorno = await _vendaAplicacao.AtualizarAsync(modelo);
            return StatusCode(StatusCodes.Status200OK, retorno);
        }
        catch (RegistroNaoEncontradoException e)
        {
            return StatusCode(StatusCodes.Status204NoContent, e.Message);
        }
        catch (StatusInvalidoException e)
        {
            return StatusCode(StatusCodes.Status204NoContent, e.Message);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }

    [HttpDelete("{guid:Guid}")]
    public async Task<ActionResult> DeletarVenda(Guid guid)
    {
        try
        {
            await _vendaAplicacao.DeletarAsync(guid);
            return StatusCode(StatusCodes.Status200OK, Messages.MessageExcluidoComSucesso);
        }
        catch (RegistroNaoEncontradoException e)
        {
            return StatusCode(StatusCodes.Status204NoContent, e.Message);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }
}
