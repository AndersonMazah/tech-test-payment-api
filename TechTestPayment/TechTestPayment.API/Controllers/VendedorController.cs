using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Interfaces;
using TechTestPayment.Dominio.Exceptions;
using TechTestPayment.Dominio.Models;
using TechTestPayment.Dominio.Utils;

namespace TechTestPayment.API.Controllers;

[Route("api/vendedor")]
[ApiController]
public class VendedorController : ControllerBase
{
    private readonly IVendedorAplicacao _vendedorAplicacao;

    public VendedorController(IVendedorAplicacao vendedorAplicacao)
    {
        _vendedorAplicacao = vendedorAplicacao;
    }

    [HttpGet]
    public async Task<ActionResult<List<VendedorDto>>> BuscarTodosVendedores()
    {
        try
        {
            List<VendedorDto> retorno = await _vendedorAplicacao.BuscarTodosAsync();
            return StatusCode(StatusCodes.Status200OK, retorno);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }

    [HttpGet("{guid:Guid}")]
    public async Task<ActionResult<VendedorDto>> BuscarVendedorPorGuid(Guid guid)
    {
        try
        {
            VendedorDto retorno = await _vendedorAplicacao.BuscarPorGuidAsync(guid);
            return StatusCode(StatusCodes.Status200OK, retorno);
        }
        catch (RegistroNaoEncontradoException e)
        {
            return StatusCode(StatusCodes.Status204NoContent, e.Message);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }

    [HttpPost]
    public async Task<ActionResult<VendedorDto>> CadastrarVendedor(CadastroVendedorModel modelo)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(StatusCodes.Status409Conflict, Messages.MessagePreencherCampos);
            }
            VendedorDto retorno = await _vendedorAplicacao.CadastrarAsync(modelo);
            return StatusCode(StatusCodes.Status201Created, retorno);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }

    [HttpPut]
    public async Task<ActionResult<VendedorDto>> AtualizarVendedor(AtualizaVendedorModel modelo)
    {
        try
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(StatusCodes.Status409Conflict, Messages.MessagePreencherCampos);
            }
            VendedorDto retorno = await _vendedorAplicacao.AtualizarAsync(modelo);
            return StatusCode(StatusCodes.Status200OK, retorno);
        }
        catch (RegistroNaoEncontradoException e)
        {
            return StatusCode(StatusCodes.Status204NoContent, e.Message);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }

    [HttpDelete("{guid:Guid}")]
    public async Task<ActionResult> DeletarVendedor(Guid guid)
    {
        try
        {
            await _vendedorAplicacao.DeletarAsync(guid);
            return StatusCode(StatusCodes.Status200OK, Messages.MessageExcluidoComSucesso);
        }
        catch (RegistroNaoEncontradoException e)
        {
            return StatusCode(StatusCodes.Status204NoContent, e.Message);
        }
        catch (Exception e)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }
}
