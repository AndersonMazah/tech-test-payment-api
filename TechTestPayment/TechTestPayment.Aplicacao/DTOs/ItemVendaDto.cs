using System;

namespace TechTestPayment.Aplicacao.DTOs;

public class ItemVendaDto
{
    public Guid ProdutoGuid { get; set; }

    public int Quantidade { get; set; }

    public decimal ValorUnitario { get; set; }

    public decimal ValorTotal { get; set; }
}