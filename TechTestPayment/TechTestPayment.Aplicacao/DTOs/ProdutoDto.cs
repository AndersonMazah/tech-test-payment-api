using System;

namespace TechTestPayment.Aplicacao.DTOs;

public class ProdutoDto
{
    public Guid Guid { get; set; }

    public string Nome { get; set; }

    public decimal Valor { get; set; }
}