using System;
using System.Collections.Generic;
using TechTestPayment.Dominio.Enumerators;

namespace TechTestPayment.Aplicacao.DTOs;

public class VendaDto
{
    public Guid Guid { get; set; }

    public string Codigo { get; set; }

    public DateTime DataVenda { get; set; }

    public StatusVenda StatusVenda { get; set; }

    public Guid VendedorGuid { get; set; }
    
    public decimal Valor { get; set; }

    public List<ItemVendaDto> ItensVenda { get; set; }
}