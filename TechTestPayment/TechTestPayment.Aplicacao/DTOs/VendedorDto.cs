﻿using System;

namespace TechTestPayment.Aplicacao.DTOs
{
    public class VendedorDto
    {
        public Guid Guid { get; set; }

        public string Cpf { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string Telefone { get; set; }
    }
}
