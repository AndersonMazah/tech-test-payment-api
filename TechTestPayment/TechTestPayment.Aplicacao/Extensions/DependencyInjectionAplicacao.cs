using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Interfaces;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;

namespace TechTestPayment.Aplicacao.Extensions;

public static class DependencyInjectionAplicacao
{
    public static IServiceCollection AddDependencyInjectionAplicacao(this IServiceCollection service)
    {
        service.AddScoped<IVendaAplicacao, VendaAplicacao>();
        service.AddScoped<IVendedorAplicacao, VendedorAplicacao>();
        service.AddScoped<IProdutoAplicacao, ProdutoAplicacao>();

        MapperConfiguration config = new MapperConfiguration(config =>
        {
            config.CreateMap<VendedorDto, Vendedor>().ReverseMap();
            config.CreateMap<ProdutoDto, Produto>().ReverseMap();
            config.CreateMap<VendaDto, Venda>().ReverseMap();
            config.CreateMap<ItemVendaDto, ItemVenda>().ReverseMap();
        });
        IMapper mapper = config.CreateMapper();
        service.AddSingleton(mapper);
        return service;
    }
}
