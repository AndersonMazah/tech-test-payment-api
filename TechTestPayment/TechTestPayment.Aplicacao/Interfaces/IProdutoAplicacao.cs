using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Aplicacao.Interfaces;

public interface IProdutoAplicacao
{
    Task<List<ProdutoDto>> BuscarTodosAsync();

    Task<ProdutoDto> BuscarPorGuidAsync(Guid guid);

    Task<ProdutoDto> CadastrarAsync(CadastroProdutoModel modelo);

    Task<ProdutoDto> AtualizarAsync(AtualizaProdutoModel modelo);

    Task DeletarAsync(Guid guid);
}