using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Aplicacao.Interfaces;

public interface IVendaAplicacao
{
    Task<List<VendaDto>> BuscarTodosAsync();

    Task<VendaDto> BuscarPorGuidAsync(Guid guid);

    Task<VendaDto> CadastrarAsync(CadastroVendaModel modelo);

    Task<VendaDto> AtualizarAsync(AtualizaVendaModel modelo);

    Task DeletarAsync(Guid guid);
}