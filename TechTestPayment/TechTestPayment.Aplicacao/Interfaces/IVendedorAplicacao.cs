using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Aplicacao.Interfaces;

public interface IVendedorAplicacao
{
    Task<List<VendedorDto>> BuscarTodosAsync();

    Task<VendedorDto> BuscarPorGuidAsync(Guid guid);

    Task<VendedorDto> CadastrarAsync(CadastroVendedorModel modelo);

    Task<VendedorDto> AtualizarAsync(AtualizaVendedorModel modelo);

    Task DeletarAsync(Guid guid);
}