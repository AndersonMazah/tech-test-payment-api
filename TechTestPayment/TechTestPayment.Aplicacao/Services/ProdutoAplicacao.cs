using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Interfaces;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Dominio.Exceptions;
using TechTestPayment.Dominio.Models;
using TechTestPayment.Dominio.Utils;
using TechTestPayment.Infra.Interfaces;

namespace TechTestPayment.Aplicacao.Services;

public class ProdutoAplicacao : IProdutoAplicacao
{
    private readonly IProdutoRepositorio _vendedorRepositorio;
    private readonly IMapper _mapper;

    public ProdutoAplicacao(IMapper mapper, IProdutoRepositorio vendedorReposiorio)
    {
        _mapper = mapper;
        _vendedorRepositorio = vendedorReposiorio;
    }

    public async Task<List<ProdutoDto>> BuscarTodosAsync()
    {
        List<Produto> vendedores = await _vendedorRepositorio.GetAllAsync();
        return _mapper.Map<List<Produto>, List<ProdutoDto>>(vendedores);
    }

    public async Task<ProdutoDto> BuscarPorGuidAsync(Guid guid)
    {
        Produto vendedor = await _vendedorRepositorio.GetByIdAsync(guid);
        if (vendedor is null)
        {
            throw new RegistroNaoEncontradoException("Produto", Messages.MessageNaoEncontrado);
        }
        return _mapper.Map<Produto, ProdutoDto>(vendedor);
    }

    public async Task<ProdutoDto> CadastrarAsync(CadastroProdutoModel modelo)
    {
        Produto vendedor = new Produto(modelo);
        await _vendedorRepositorio.AddAsync(vendedor);
        await _vendedorRepositorio.CommitAsync();
        return _mapper.Map<Produto, ProdutoDto>(vendedor);
    }

    public async Task<ProdutoDto> AtualizarAsync(AtualizaProdutoModel modelo)
    {
        Produto vendedor = await _vendedorRepositorio.GetByIdAsync(modelo.Guid);
        if (vendedor is null)
        {
            throw new RegistroNaoEncontradoException("Produto", Messages.MessageNaoEncontrado);
        }
        vendedor.Atualizar(modelo);
        _vendedorRepositorio.Update(vendedor);
        await _vendedorRepositorio.CommitAsync();
        return _mapper.Map<Produto, ProdutoDto>(vendedor);
    }

    public async Task DeletarAsync(Guid guid)
    {
        try
        {
            Produto vendedor = await _vendedorRepositorio.GetByIdAsync(guid);
            if (vendedor is null)
            {
                throw new RegistroNaoEncontradoException("Produto", Messages.MessageNaoEncontrado);
            }
            _vendedorRepositorio.Delete(vendedor);
            await _vendedorRepositorio.CommitAsync();
        }
        catch (Exception)
        {
            throw;
        }
    }
}