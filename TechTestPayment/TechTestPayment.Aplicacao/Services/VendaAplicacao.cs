using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Interfaces;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Dominio.Enumerators;
using TechTestPayment.Dominio.Exceptions;
using TechTestPayment.Dominio.Models;
using TechTestPayment.Dominio.Utils;
using TechTestPayment.Infra.Interfaces;

namespace TechTestPayment.Aplicacao.Services;

public class VendaAplicacao : IVendaAplicacao
{
    private readonly IVendaRepositorio _vendaRepositorio;
    private readonly IProdutoRepositorio _produtoRepositorio;
    private readonly IMapper _mapper;

    public VendaAplicacao(IMapper mapper, IVendaRepositorio vendaReposiorio, IProdutoRepositorio produtoRepositorio)
    {
        _mapper = mapper;
        _vendaRepositorio = vendaReposiorio;
        _produtoRepositorio = produtoRepositorio;
    }

    public async Task<List<VendaDto>> BuscarTodosAsync()
    {
        List<Venda> vendaes = await _vendaRepositorio.GetAllVendasComItensVendaAsync();
        return _mapper.Map<List<Venda>, List<VendaDto>>(vendaes);
    }

    public async Task<VendaDto> BuscarPorGuidAsync(Guid guid)
    {
        Venda venda = await _vendaRepositorio.GetByIdAsync(guid);
        if (venda is null)
        {
            throw new RegistroNaoEncontradoException("Venda", Messages.MessageNaoEncontrado);
        }
        return _mapper.Map<Venda, VendaDto>(venda);
    }

    public async Task<VendaDto> CadastrarAsync(CadastroVendaModel modelo)
    {
        if (!modelo.ItensVenda.Any())
        {
            throw new RegistroNaoEncontradoException("Venda", "A venda deve conter ao menos 1 item");
        }
        Venda venda = new Venda(modelo);
        await _vendaRepositorio.AddAsync(venda);

        foreach (ItemVendaModel itemVendaModel in modelo.ItensVenda)
        {
            Produto produto = await _produtoRepositorio.GetByIdAsync(itemVendaModel.ProdutoGuid);
            if (produto is null)
            {
                throw new RegistroNaoEncontradoException("Produto", "O produto nao foi encontrado!");
            }
            ItemVenda itemVenda = new ItemVenda(Guid.NewGuid(), venda.Guid, produto.Guid, itemVendaModel.Quantidade, produto.Valor, itemVendaModel.Quantidade * produto.Valor);
            venda.ItensVenda.Add(itemVenda);
        }
        venda.CalcularValorDaVenda();
        await _vendaRepositorio.CommitAsync();
        return _mapper.Map<Venda, VendaDto>(venda);
    }

    public async Task<VendaDto> AtualizarAsync(AtualizaVendaModel modelo)
    {
        try
        {
            Venda venda = await _vendaRepositorio.GetByIdAsync(modelo.Guid);
            if (venda is null)
            {
                throw new RegistroNaoEncontradoException("Venda", Messages.MessageNaoEncontrado);
            }
            if (!ValidarAtualizarDeStatus(venda.StatusVenda, modelo.StatusVenda))
            {
                throw new StatusInvalidoException("Venda", Messages.MessageStatusInvalido);
            }
            venda.Atualizar(modelo);
            _vendaRepositorio.Update(venda);
            await _vendaRepositorio.CommitAsync();
            return _mapper.Map<Venda, VendaDto>(venda);
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task DeletarAsync(Guid guid)
    {
        try
        {
            Venda venda = await _vendaRepositorio.GetByIdAsync(guid);
            if (venda is null)
            {
                throw new RegistroNaoEncontradoException("Venda", Messages.MessageNaoEncontrado);
            }
            _vendaRepositorio.Delete(venda);
            await _vendaRepositorio.CommitAsync();
        }
        catch (Exception)
        {
            throw;
        }
    }

    private bool ValidarAtualizarDeStatus(StatusVenda statusVendaAtual, StatusVenda statusVendaDesejado)
    {
        switch (statusVendaAtual)
        {
            case StatusVenda.AguardandoPagamento:
                if (statusVendaDesejado == StatusVenda.PagamentoAprovado || statusVendaDesejado == StatusVenda.Cancelada)
                {
                    return true;
                }
                break;
            case StatusVenda.PagamentoAprovado:
                if (statusVendaDesejado == StatusVenda.EnviadoParaTransportadora || statusVendaDesejado == StatusVenda.Cancelada)
                {
                    return true;
                }
                break;
            case StatusVenda.EnviadoParaTransportadora:
                if (statusVendaDesejado == StatusVenda.Cancelada)
                {
                    return true;
                }
                break;
        }
        return false;
    }
}