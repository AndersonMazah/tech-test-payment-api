using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Interfaces;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Dominio.Exceptions;
using TechTestPayment.Dominio.Models;
using TechTestPayment.Dominio.Utils;
using TechTestPayment.Infra.Interfaces;

namespace TechTestPayment.Aplicacao.Services;

public class VendedorAplicacao : IVendedorAplicacao
{
    private readonly IVendedorRepositorio _vendedorRepositorio;
    private readonly IMapper _mapper;

    public VendedorAplicacao(IMapper mapper, IVendedorRepositorio vendedorReposiorio)
    {
        _mapper = mapper;
        _vendedorRepositorio = vendedorReposiorio;
    }

    public async Task<List<VendedorDto>> BuscarTodosAsync()
    {
        List<Vendedor> vendedores = await _vendedorRepositorio.GetAllAsync();
        return _mapper.Map<List<Vendedor>, List<VendedorDto>>(vendedores);
    }

    public async Task<VendedorDto> BuscarPorGuidAsync(Guid guid)
    {
        Vendedor vendedor = await _vendedorRepositorio.GetByIdAsync(guid);
        if (vendedor is null)
        {
            throw new RegistroNaoEncontradoException("Vendedor", Messages.MessageNaoEncontrado);
        }
        return _mapper.Map<Vendedor, VendedorDto>(vendedor);
    }

    public async Task<VendedorDto> CadastrarAsync(CadastroVendedorModel modelo)
    {
        Vendedor vendedor = new Vendedor(modelo);
        await _vendedorRepositorio.AddAsync(vendedor);
        await _vendedorRepositorio.CommitAsync();
        return _mapper.Map<Vendedor, VendedorDto>(vendedor);
    }

    public async Task<VendedorDto> AtualizarAsync(AtualizaVendedorModel modelo)
    {
        Vendedor vendedor = await _vendedorRepositorio.GetByIdAsync(modelo.Guid);
        if (vendedor is null)
        {
            throw new RegistroNaoEncontradoException("Vendedor", Messages.MessageNaoEncontrado);
        }
        vendedor.Atualizar(modelo);
        _vendedorRepositorio.Update(vendedor);
        await _vendedorRepositorio.CommitAsync();
        return _mapper.Map<Vendedor, VendedorDto>(vendedor);
    }

    public async Task DeletarAsync(Guid guid)
    {
        try
        {
            Vendedor vendedor = await _vendedorRepositorio.GetByIdAsync(guid);
            if (vendedor is null)
            {
                throw new RegistroNaoEncontradoException("Vendedor", Messages.MessageNaoEncontrado);
            }
            _vendedorRepositorio.Delete(vendedor);
            await _vendedorRepositorio.CommitAsync();
        }
        catch (Exception)
        {
            throw;
        }
    }
}