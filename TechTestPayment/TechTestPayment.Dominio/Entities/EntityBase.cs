using System;

namespace TechTestPayment.Dominio.Entities;

public abstract class EntityBase
{
    public Guid Guid { get; protected set; }
}