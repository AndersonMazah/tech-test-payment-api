using System;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Dominio.Entities;

public class ItemVenda : EntityBase
{
    public Guid VendaGuid { get; private set; }

    public Guid ProdutoGuid { get; private set; }

    public int Quantidade { get; private set; }

    public decimal ValorUnitario { get; private set; }

    public decimal ValorTotal { get; private set; }

    public Produto Produto { get; private set; }

    public virtual Venda Venda { get; private set; }

    public ItemVenda(Guid guid, Guid vendaGuid, Guid produtoGuid, int quantidade, decimal valorUnitario, decimal valorTotal)
    {
        Guid = guid;
        VendaGuid = vendaGuid;
        ProdutoGuid = produtoGuid;
        Quantidade = quantidade;
        ValorUnitario = valorUnitario;
        ValorTotal = valorTotal;
    }

    public ItemVenda(ItemVendaModel modelo)
    {
        Quantidade = modelo.Quantidade;
    }
}