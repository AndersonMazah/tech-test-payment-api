using System;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Dominio.Entities;

public class Produto : EntityBase
{
    public string Nome { get; private set; }

    public decimal Valor { get; private set; }

    public Produto(Guid guid, string nome, decimal valor)
    {
        Guid = guid;
        Nome = nome;
        Valor = valor;
    }

    public Produto(CadastroProdutoModel modelo)
    {
        Guid = Guid.NewGuid();
        Nome = modelo.Nome;
        Valor = modelo.Valor;
    }

    public void Atualizar(AtualizaProdutoModel modelo)
    {
        Nome = modelo.Nome;
        Valor = modelo.Valor;
    }
}