using System;
using System.Collections.Generic;
using TechTestPayment.Dominio.Enumerators;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Dominio.Entities;

public class Venda : EntityBase
{
    public Guid VendedorGuid { get; private set; }

    public string Codigo { get; private set; }

    public DateTime DataVenda { get; private set; }

    public StatusVenda StatusVenda { get; private set; }

    public decimal Valor { get; private set; }

    public List<ItemVenda> ItensVenda { get; set; } = new List<ItemVenda>();

    public Venda(Guid guid, string codigo, DateTime dataVenda, StatusVenda statusVenda, decimal valor)
    {
        Guid = guid;
        Codigo = codigo;
        DataVenda = dataVenda;
        StatusVenda = statusVenda;
        Valor = valor;
    }

    public Venda(CadastroVendaModel modelo)
    {
        Guid = Guid.NewGuid();
        Codigo = modelo.Codigo;
        DataVenda = modelo.DataVenda;
        StatusVenda = StatusVenda.AguardandoPagamento;
        VendedorGuid = modelo.VendedorGuid;
    }

    public void Atualizar(AtualizaVendaModel modelo)
    {
        StatusVenda = modelo.StatusVenda;
    }

    public void CalcularValorDaVenda()
    {
        Valor = 0;
        foreach (ItemVenda itemVenda in ItensVenda)
        {
            Valor += itemVenda.ValorTotal;
        }
    }
}