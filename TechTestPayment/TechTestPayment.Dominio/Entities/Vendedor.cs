using System;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Dominio.Entities;

public class Vendedor : EntityBase
{
    public string Cpf { get; private set; }

    public string Nome { get; private set; }

    public string Email { get; private set; }

    public string Telefone { get; private set; }

    public Vendedor(Guid guid, string cpf, string nome, string email, string telefone)
    {
        Guid = guid;
        Cpf = cpf;
        Nome = nome;
        Email = email;
        Telefone = telefone;
    }

    public Vendedor(CadastroVendedorModel modelo)
    {
        Guid = Guid.NewGuid();
        Cpf = modelo.Cpf;
        Nome = modelo.Nome;
        Email = modelo.Email;
        Telefone = modelo.Telefone;
    }

    public void Atualizar(AtualizaVendedorModel modelo)
    {
        Nome = modelo.Nome;
        Email = modelo.Email;
        Telefone = modelo.Telefone;
    }
}