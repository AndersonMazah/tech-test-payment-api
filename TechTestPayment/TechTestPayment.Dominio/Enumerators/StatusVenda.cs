using System.ComponentModel;

namespace TechTestPayment.Dominio.Enumerators;

public enum StatusVenda : byte
{
    [Description("Aguardando Pagamento")]
    AguardandoPagamento = 0,

    [Description("Pagamento Aprovado")]
    PagamentoAprovado = 1,

    [Description("Enviado para Transportadora")]
    EnviadoParaTransportadora = 2,

    [Description("Entregue")]
    Entregue = 3,

    [Description("Cancelada")]
    Cancelada = 4
}