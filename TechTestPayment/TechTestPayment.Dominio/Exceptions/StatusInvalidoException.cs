﻿namespace TechTestPayment.Dominio.Exceptions;

public class StatusInvalidoException : ExceptionBase
{
    public StatusInvalidoException(string titulo, string mensagem) : base(titulo, mensagem)
    {
    }
}