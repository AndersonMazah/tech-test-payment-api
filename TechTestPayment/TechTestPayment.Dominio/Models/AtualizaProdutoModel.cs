using System;
using System.ComponentModel.DataAnnotations;
using TechTestPayment.Dominio.Models.Validators;

namespace TechTestPayment.Dominio.Models;

public class AtualizaProdutoModel
{
    [Display(Name = "Guid do Produto")]
    [GuidValidator]
    public Guid Guid { get; set; }

    [Display(Name = "Nome")]
    [StringValidator(MaxLength = 50)]
    public string Nome { get; set; }

    [Display(Name = "Valor")]
    [DecimalValidator]
    public decimal Valor { get; set; }
}