using System;
using System.ComponentModel.DataAnnotations;
using TechTestPayment.Dominio.Enumerators;
using TechTestPayment.Dominio.Models.Validators;

namespace TechTestPayment.Dominio.Models;

public class AtualizaVendaModel
{
    [Display(Name = "Guid da Venda")]
    [GuidValidator]
    public Guid Guid { get; set; }

    [Display(Name = "Status da Venda")]
    [ByteValidator(MinValue = 1, MaxValue = 4)]
    public StatusVenda StatusVenda { get; set; }
}