using System;
using System.ComponentModel.DataAnnotations;
using TechTestPayment.Dominio.Models.Validators;

namespace TechTestPayment.Dominio.Models;

public class AtualizaVendedorModel
{
    [Display(Name = "Guid do Vendedor")]
    [GuidValidator]
    public Guid Guid { get; set; }

    [Display(Name = "Nome")]
    [StringValidator(MaxLength = 150)]
    public string Nome { get; set; }

    [Display(Name = "E-mail")]
    [EmailValidator(MaxLength = 150)]
    public string Email { get; set; }

    [Display(Name = "Telefone")]
    [TelefoneValidator]
    public string Telefone { get; set; }
}