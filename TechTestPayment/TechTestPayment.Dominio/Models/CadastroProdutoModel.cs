using System.ComponentModel.DataAnnotations;
using TechTestPayment.Dominio.Models.Validators;

namespace TechTestPayment.Dominio.Models;

public class CadastroProdutoModel
{
    [Display(Name = "Nome")]
    [StringValidator(MaxLength = 50)]
    public string Nome { get; set; }

    [Display(Name = "Valor")]
    [DecimalValidator(MinValue = 0)]
    public decimal Valor { get; set; }
}