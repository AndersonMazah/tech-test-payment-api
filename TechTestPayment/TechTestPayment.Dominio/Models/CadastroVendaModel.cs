using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TechTestPayment.Dominio.Models.Validators;

namespace TechTestPayment.Dominio.Models;

public class CadastroVendaModel
{
    [Display(Name = "Data da Venda")]
    [DataValidator]
    public DateTime DataVenda { get; set; }

    [Display(Name = "Codigo da Venda")]
    [StringValidator(MaxLength = 10)]
    public string Codigo { get; set; }

    [Display(Name = "Guid do Vendedor")]
    [GuidValidator]
    public Guid VendedorGuid { get; set; }

    [Display(Name = "Itens da Venda")]
    [Required]
    public List<ItemVendaModel> ItensVenda { get; set; }
}