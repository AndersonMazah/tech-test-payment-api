using System;
using System.ComponentModel.DataAnnotations;
using TechTestPayment.Dominio.Models.Validators;

namespace TechTestPayment.Dominio.Models;

public class ItemVendaModel
{
    [Display(Name = "Produto")]
    [GuidValidator]
    public Guid ProdutoGuid { get; set; }

    [Display(Name = "Quantidade")]
    [IntValidator(MinValue = 1)]
    public int Quantidade { get; set; }
}