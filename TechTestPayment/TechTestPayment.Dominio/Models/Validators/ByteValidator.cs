using System;

namespace TechTestPayment.Dominio.Models.Validators;

public class ByteValidator : ValidatorBase
{
    public byte MinValue { get; set; } = byte.MinValue;

    public byte MaxValue { get; set; } = byte.MaxValue;

    public override bool IsValid(object value)
    {
        if (value is null)
        {
            if (Required == true)
            {
                ErrorMessage = "O campo {0} é obrigatório!";
                return false;
            }
            return true;
        }
        byte valor = Convert.ToByte(value);
        if (valor < MinValue || valor > MaxValue)
        {
            ErrorMessage = $"O campo {{0}} deve estar entre {MinValue} e {MaxValue}!";
            return false;
        }
        return true;
    }
}
