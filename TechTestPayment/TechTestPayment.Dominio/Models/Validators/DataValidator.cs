using System;

namespace TechTestPayment.Dominio.Models.Validators;

public class DataValidator : ValidatorBase
{
    public override bool IsValid(object value)
    {
        if (Required == true && (DateTime)value == DateTime.MinValue)
        {
            ErrorMessage = "O campo {0} é obrigatório!";
            return false;
        }
        return true;
    }
}