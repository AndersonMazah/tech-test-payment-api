using System.Text.RegularExpressions;

namespace TechTestPayment.Dominio.Models.Validators;

public class EmailValidator : ValidatorBase
{
    public byte MaxLength { get; set; } = byte.MaxValue;

    public override bool IsValid(object value)
    {
        string email = value as string;
        if (string.IsNullOrWhiteSpace(email))
        {
            if (Required == true)
            {
                ErrorMessage = "O campo {0} é obrigatório!";
                return false;
            }
            return true;
        }
        if (email.Length > MaxLength)
        {
            ErrorMessage = $"O campo {{0}} deve ter no máximo {MaxLength} caracteres!";
            return false;
        }

        Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        Match match = regex.Match(email);
        if (!match.Success)
        {
            ErrorMessage = "O campo {0} está inválido!";
            return false;
        }
        return true;
    }
}
