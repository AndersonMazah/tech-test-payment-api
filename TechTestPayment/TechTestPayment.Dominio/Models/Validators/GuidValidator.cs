using System;

namespace TechTestPayment.Dominio.Models.Validators;

public class GuidValidator : ValidatorBase
{
    public override bool IsValid(object value)
    {
        if ((Guid)value == Guid.Empty)
        {
            if (Required == true)
            {
                ErrorMessage = "O campo {0} é obrigatório!";
                return false;
            }
        }
        return true;
    }
}
