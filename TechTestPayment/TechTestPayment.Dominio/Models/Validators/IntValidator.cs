using System;

namespace TechTestPayment.Dominio.Models.Validators;

public class IntValidator : ValidatorBase
{
    public int MinValue { get; set; } = int.MinValue;

    public int MaxValue { get; set; } = int.MaxValue;

    public override bool IsValid(object value)
    {
        if (value is null)
        {
            if (Required == true)
            {
                ErrorMessage = "O campo {0} é obrigatório!";
                return false;
            }
            return true;
        }
        int valor = Convert.ToInt32(value);
        if (valor < MinValue || valor > MaxValue)
        {
            ErrorMessage = $"O campo {{0}} deve estar entre {MinValue} e {MaxValue}!";
            return false;
        }
        return true;
    }
}