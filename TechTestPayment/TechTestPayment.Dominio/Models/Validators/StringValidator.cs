namespace TechTestPayment.Dominio.Models.Validators;

public class StringValidator : ValidatorBase
{
    public ushort MaxLength { get; set; } = ushort.MaxValue;

    public override bool IsValid(object value)
    {
        string texto = value as string;
        if (string.IsNullOrWhiteSpace(texto))
        {
            if (Required == true)
            {
                ErrorMessage = "O campo {0} é obrigatório!";
                return false;
            }
            return true;
        }
        if (texto.Length > MaxLength)
        {
            ErrorMessage = $"O campo {{0}} deve ter no máximo {MaxLength} caracteres!";
            return false;
        }
        return true;
    }
}
