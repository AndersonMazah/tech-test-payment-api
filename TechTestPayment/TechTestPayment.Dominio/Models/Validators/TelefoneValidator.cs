using System.Text.RegularExpressions;

namespace TechTestPayment.Dominio.Models.Validators;

public class TelefoneValidator : ValidatorBase
{
    public short MaxLength { get; set; } = 12;

    public override bool IsValid(object value)
    {
        string telefone = value as string;
        if (string.IsNullOrWhiteSpace(telefone))
        {
            if (Required == true)
            {
                ErrorMessage = "O campo {0} é obrigatório!";
                return false;
            }
            return true;
        }
        if (telefone.Length > MaxLength)
        {
            ErrorMessage = $"O campo {{0}} deve ter no máximo {MaxLength} caracteres!";
            return false;
        }

        Regex regex = new Regex(@"^[0-9]{1,12}$");
        Match match = regex.Match(telefone);
        if (!match.Success)
        {
            ErrorMessage = "O campo {0} está inválido!";
            return false;
        }
        return true;
    }
}
