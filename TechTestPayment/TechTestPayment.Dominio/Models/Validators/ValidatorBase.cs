using System.ComponentModel.DataAnnotations;

namespace TechTestPayment.Dominio.Models.Validators;

public class ValidatorBase : ValidationAttribute
{
    public bool Required { get; set; } = true;
    public string Message { get; set; } = string.Empty;

    public override bool IsValid(object value)
    {
        return base.IsValid(value);
    }
}