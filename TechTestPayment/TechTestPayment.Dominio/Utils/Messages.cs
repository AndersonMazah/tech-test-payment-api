﻿namespace TechTestPayment.Dominio.Utils;
public static class Messages
{
    public const string MessageExcluidoComSucesso = "Excluido com sucesso.";

    public const string MessageNaoEncontrado = "Registro não encontrado.";

    public const string MessagePreencherCampos = "Os campos devem ser preenchidos.";

    public const string MessageStatusInvalido = "Não é possível alterar o status da venda para o status selecionado!";
}