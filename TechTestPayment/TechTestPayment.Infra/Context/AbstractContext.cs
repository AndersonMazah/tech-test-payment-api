﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace TechTestPayment.Infra.Context;

public class AbstractContext : DbContext
{
    protected string ConnectionString { get; set; }

    protected Assembly ConfigurationAssembly => GetConfigurationAssembly();

    protected Func<Type, bool> ConfigurationTypePredicate => GetConfigurationTypePredicate();

    public AbstractContext(string conn)
    {
        string conection = conn ?? throw new ArgumentException("Não foi possível conectar ao banco de dados", new Exception());
        ConnectionString = SetConnectionString(conection);
    }

    private AbstractContext() { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        if (ConfigurationAssembly == null)
        {
            throw new ArgumentException("O Assembly de configuração do contexto deve ser definido", new Exception());
        }

        modelBuilder.ApplyConfigurationsFromAssembly(ConfigurationAssembly, ConfigurationTypePredicate ?? null);
        base.OnModelCreating(modelBuilder);
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer(ConnectionString);
    }

    protected virtual Assembly GetConfigurationAssembly()
    {
        return GetType().Assembly;
    }

    protected virtual Func<Type, bool> GetConfigurationTypePredicate()
    {
        return null;
    }

    private string SetConnectionString(string value) => value;

    public bool Commit() => SaveChanges() > 0;

    public async Task<bool> CommitAsync() => await SaveChangesAsync().ConfigureAwait(true) > 0;
}
