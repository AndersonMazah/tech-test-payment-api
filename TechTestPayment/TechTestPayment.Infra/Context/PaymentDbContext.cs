using Microsoft.EntityFrameworkCore;
using TechTestPayment.Dominio.Entities;

namespace TechTestPayment.Infra.Context;

public class PaymentDbContext : AbstractContext
{
    public PaymentDbContext() : base("Server=localhost;Database=TechTestPayment;User Id=sa;Password=1q2w3e4r@#$;")
    {
    }

    public DbSet<Vendedor> Vendedores { get; set; }

    public DbSet<Produto> Produtos { get; set; }

    public DbSet<ItemVenda> ItemVendas { get; set; }

    public DbSet<Venda> Vendas { get; set; }
}
