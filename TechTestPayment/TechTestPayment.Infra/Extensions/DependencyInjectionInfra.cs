using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TechTestPayment.Infra.Context;
using TechTestPayment.Infra.Interfaces;
using TechTestPayment.Infra.Repositorios;

namespace TechTestPayment.Infra.Extensions;

public static class DependencyInjectionInfra
{
    public static IServiceCollection AddDependencyInjectionInfra(this IServiceCollection service)
    {
        service.AddScoped<DbContext, PaymentDbContext>();
        service.AddScoped<IVendaRepositorio, VendaRepositorio>();
        service.AddScoped<IVendedorRepositorio, VendedorRepositorio>();
        service.AddScoped<IProdutoRepositorio, ProdutoRepositorio>();
        return service;
    }
}