using TechTestPayment.Dominio.Entities;

namespace TechTestPayment.Infra.Interfaces;

public interface IProdutoRepositorio : IRepositorioBase<Produto>
{
}