﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TechTestPayment.Infra.Interfaces;

public interface IRepositorioBase<TEntity>
{
    void Add(TEntity entity);

    Task AddAsync(TEntity entity);

    void Disposes();

    Task<List<TEntity>> GetAllAsync();

    TEntity GetById(Guid guid);

    Task<TEntity> GetByIdAsync(Guid guid);

    bool Commit();

    Task<bool> CommitAsync();

    void Update(TEntity entity);

    void Delete(TEntity entity);

    void DeleteRange(IEnumerable<TEntity> entity);
}
