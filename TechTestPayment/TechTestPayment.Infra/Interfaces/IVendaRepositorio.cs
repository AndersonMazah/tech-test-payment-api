using System.Collections.Generic;
using System.Threading.Tasks;
using TechTestPayment.Dominio.Entities;

namespace TechTestPayment.Infra.Interfaces;

public interface IVendaRepositorio : IRepositorioBase<Venda>
{
    Task<List<Venda>> GetAllVendasComItensVendaAsync();
}