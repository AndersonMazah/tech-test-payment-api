using TechTestPayment.Dominio.Entities;

namespace TechTestPayment.Infra.Interfaces;

public interface IVendedorRepositorio : IRepositorioBase<Vendedor>
{
}