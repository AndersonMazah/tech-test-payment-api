﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TechTestPayment.Dominio.Entities;

namespace TechTestPayment.Infra.Mapeamentos;

public class ItemVendaMapeamento : IEntityTypeConfiguration<ItemVenda>
{
    public void Configure(EntityTypeBuilder<ItemVenda> builder)
    {
        builder.ToTable("itemvenda");

        builder.HasKey(v => v.Guid);

        builder.Property(v => v.Guid).HasColumnName("id").HasColumnType("uniqueidentifier");
        builder.Property(v => v.VendaGuid).HasColumnName("vendaid").HasColumnType("uniqueidentifier");
        builder.Property(v => v.ProdutoGuid).HasColumnName("produtoid").HasColumnType("uniqueidentifier");
        builder.Property(v => v.Quantidade).HasColumnName("quantidade").HasColumnType("int");
        builder.Property(v => v.ValorUnitario).HasColumnName("valorunitario").HasColumnType("decimal(16,2)");
        builder.Property(v => v.ValorTotal).HasColumnName("valortotal").HasColumnType("decimal(16,2)");
    }
}
