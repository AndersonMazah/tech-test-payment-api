﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TechTestPayment.Dominio.Entities;

namespace TechTestPayment.Infra.Mapeamentos;

public class ProdutoMapeamento : IEntityTypeConfiguration<Produto>
{
    public void Configure(EntityTypeBuilder<Produto> builder)
    {
        builder.ToTable("produto");

        builder.HasKey(v => v.Guid);

        builder.Property(v => v.Guid).HasColumnName("id").HasColumnType("uniqueidentifier");
        builder.Property(v => v.Nome).HasColumnName(name: "nome").HasColumnType("varchar(50)");
        builder.Property(v => v.Valor).HasColumnName("valor").HasColumnType("decimal(16,2)");
    }
}