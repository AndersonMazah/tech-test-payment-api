﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TechTestPayment.Dominio.Entities;

namespace TechTestPayment.Infra.Mapeamentos;

public class VendaMapeamento : IEntityTypeConfiguration<Venda>
{
    public void Configure(EntityTypeBuilder<Venda> builder)
    {
        builder.ToTable("venda");

        builder.HasKey(v => v.Guid);

        builder.Property(v => v.Guid).HasColumnName("id").HasColumnType("uniqueidentifier");
        builder.Property(v => v.DataVenda).HasColumnName("datavenda").HasColumnType("datetime");
        builder.Property(v => v.StatusVenda).HasColumnName("statusvenda").HasColumnType("int");
        builder.Property(v => v.VendedorGuid).HasColumnName("VendedorId").HasColumnType("uniqueidentifier");
        builder.Property(v => v.Valor).HasColumnName("valor").HasColumnType("decimal(16,2)");

        builder.HasMany(v => v.ItensVenda).WithOne(v =>v.Venda).HasForeignKey(i => i.VendaGuid);
    }
}
