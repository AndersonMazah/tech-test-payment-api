﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TechTestPayment.Dominio.Entities;

namespace TechTestPayment.Infra.Mapeamentos;

public class VendedorMapeamento : IEntityTypeConfiguration<Vendedor>
{
    public void Configure(EntityTypeBuilder<Vendedor> builder)
    {
        builder.ToTable("vendedor");

        builder.HasKey(v => v.Guid);

        builder.Property(v => v.Guid).HasColumnName("id").HasColumnType("uniqueidentifier");
        builder.Property(v => v.Cpf).HasColumnName("cpf").HasColumnType("varchar(14)");
        builder.Property(v => v.Nome).HasColumnName("nome").HasColumnType("varchar(150)");
        builder.Property(v => v.Email).HasColumnName("email").HasColumnType("varchar(150)");
        builder.Property(v => v.Telefone).HasColumnName("telefone").HasColumnType("varchar(15)");
    }
}
