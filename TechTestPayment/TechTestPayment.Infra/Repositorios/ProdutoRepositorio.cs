using Microsoft.EntityFrameworkCore;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Infra.Interfaces;

namespace TechTestPayment.Infra.Repositorios;

public class ProdutoRepositorio : RepositorioBase<Produto>, IProdutoRepositorio
{
    public ProdutoRepositorio(DbContext context) : base(context)
    {
    }
}