﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TechTestPayment.Infra.Interfaces;

namespace TechTestPayment.Infra.Repositorios;

public class RepositorioBase<TEntity> : IRepositorioBase<TEntity> where TEntity : class
{
    protected DbContext Db { get; private set; }

    protected DbSet<TEntity> DbSet { get; }

    public RepositorioBase(DbContext context)
    {
        Db = context ?? throw new ArgumentException("Falha ao inicializar o repositorio. Contexto inválido.");
        DbSet = Db.Set<TEntity>();
    }

    public void Disposes()
    {
        Db.Dispose();
    }

    public virtual void Add(TEntity entity)
    {
        Db.Entry(entity).State = EntityState.Added;
        DbSet.Add(entity);
    }

    public async virtual Task AddAsync(TEntity entity)
    {
        Db.Entry(entity).State = EntityState.Added;
        await Db.AddAsync(entity);
    }

    public async virtual Task<List<TEntity>> GetAllAsync()
    {
        return await DbSet.ToListAsync();
    }

    public virtual TEntity GetById(Guid guid)
    {
        return DbSet.Find(guid);
    }

    public async virtual Task<TEntity> GetByIdAsync(Guid guid)
    {
        return await DbSet.FindAsync(guid);
    }

    public bool Commit()
    {
        return Db.SaveChanges() > 0;
    }

    public async Task<bool> CommitAsync()
    {
        return await Db.SaveChangesAsync().ConfigureAwait(true) > 0;
    }

    public void Update(TEntity entity)
    {
        Db.Entry(entity).State = EntityState.Modified;
        Db.Update(entity);
    }

    public void Delete(TEntity entity)
    {
        Db.Entry(entity).State = EntityState.Deleted;
        DbSet.Remove(entity);
    }

    public void DeleteRange(IEnumerable<TEntity> entity)
    {
        Db.Entry(entity).State = EntityState.Deleted;
        DbSet.RemoveRange(entity);
    }
}