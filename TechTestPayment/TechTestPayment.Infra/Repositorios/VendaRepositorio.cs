using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Infra.Interfaces;

namespace TechTestPayment.Infra.Repositorios;

public class VendaRepositorio : RepositorioBase<Venda>, IVendaRepositorio
{
    public VendaRepositorio(DbContext context) : base(context)
    {
    }

    public async Task<List<Venda>> GetAllVendasComItensVendaAsync()
    {
        return await DbSet.Include(i => i.ItensVenda).ToListAsync();
    }
}