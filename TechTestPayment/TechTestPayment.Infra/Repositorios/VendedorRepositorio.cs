using Microsoft.EntityFrameworkCore;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Infra.Interfaces;

namespace TechTestPayment.Infra.Repositorios;

public class VendedorRepositorio : RepositorioBase<Vendedor>, IVendedorRepositorio
{
    public VendedorRepositorio(DbContext context) : base(context)
    {
    }
}