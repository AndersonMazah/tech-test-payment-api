﻿using Moq;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Test.ProdutoAplicacaoTest;

public class AtualizarProdutoTest : IClassFixture<ProdutoAplicacaoFixture>
{
    private readonly ProdutoAplicacaoFixture _fixture;

    public AtualizarProdutoTest(ProdutoAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void Atualizar()
    {
        Produto produto = new Produto(_fixture.GuidProduto, "Nome do Produto", 12.34M);
        _fixture.produtoRepositorioMock.Setup(v => v.GetByIdAsync(_fixture.GuidProduto)).ReturnsAsync(produto);
        _fixture.produtoRepositorioMock.Setup(v => v.Update(produto));
        _fixture.produtoRepositorioMock.Setup(v => v.CommitAsync());

        ProdutoAplicacao produtoAplicacao = _fixture.MakeSut();
        AtualizaProdutoModel modelo = new AtualizaProdutoModel();
        modelo.Guid = _fixture.GuidProduto;

        Task<ProdutoDto> retorno = produtoAplicacao.AtualizarAsync(modelo);
        Assert.NotNull(retorno.Result);
        Assert.Equal(_fixture.GuidProduto, retorno.Result.Guid);
    }
}
