﻿using Moq;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;

namespace TechTestPayment.Test.ProdutoAplicacaoTest;

public class BuscarProdutoPorGuidTest : IClassFixture<ProdutoAplicacaoFixture>
{
    private readonly ProdutoAplicacaoFixture _fixture;

    public BuscarProdutoPorGuidTest(ProdutoAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void BuscarPorGuid()
    {
        Produto produto = new Produto(_fixture.GuidProduto, "Nome do Produto", 12.34M);
        _fixture.produtoRepositorioMock.Setup(v => v.GetByIdAsync(_fixture.GuidProduto)).ReturnsAsync(produto);

        ProdutoAplicacao produtoAplicacao = _fixture.MakeSut();

        Task<ProdutoDto> retorno = produtoAplicacao.BuscarPorGuidAsync(_fixture.GuidProduto);
        Assert.NotNull(retorno.Result);
        Assert.Equal(_fixture.GuidProduto, retorno.Result.Guid);
    }
}
