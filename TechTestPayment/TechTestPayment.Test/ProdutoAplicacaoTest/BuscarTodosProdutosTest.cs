﻿using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;

namespace TechTestPayment.Test.ProdutoAplicacaoTest;

public class BuscarTodosProdutosTest : IClassFixture<ProdutoAplicacaoFixture>
{
    private readonly ProdutoAplicacaoFixture _fixture;

    public BuscarTodosProdutosTest(ProdutoAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void BuscarTodos()
    {
        Produto produto = new Produto(_fixture.GuidProduto, "Nome do Produto", 12.34M);
        _fixture.produtoRepositorioMock.Setup(v => v.GetAllAsync()).ReturnsAsync(new List<Produto>() { produto });

        ProdutoAplicacao produtoAplicacao = _fixture.MakeSut();

        Task<List<ProdutoDto>> retorno = produtoAplicacao.BuscarTodosAsync();
        Assert.NotNull(retorno.Result);
        Assert.Single(retorno.Result);
        Assert.True(retorno.Result.Where(v => v.Guid == _fixture.GuidProduto).Any());
    }
}
