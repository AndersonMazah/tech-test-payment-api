﻿using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Test.ProdutoAplicacaoTest;

public class CadastrarProdutoTest : IClassFixture<ProdutoAplicacaoFixture>
{
    private readonly ProdutoAplicacaoFixture _fixture;

    public CadastrarProdutoTest(ProdutoAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void Cadastrar()
    {
        decimal valor = 12.34M;
        Produto produto = new Produto(_fixture.GuidProduto, "Nome do Produto", 12.34M);
        _fixture.produtoRepositorioMock.Setup(v => v.AddAsync(produto));
        _fixture.produtoRepositorioMock.Setup(v => v.CommitAsync());

        ProdutoAplicacao produtoAplicacao = _fixture.MakeSut();
        CadastroProdutoModel modelo = new CadastroProdutoModel();
        modelo.Valor = valor;

        Task<ProdutoDto> retorno = produtoAplicacao.CadastrarAsync(modelo);
        Assert.NotNull(retorno.Result);
        Assert.True(retorno.Result.Valor == valor);
    }
}
