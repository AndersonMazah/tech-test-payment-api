﻿using Moq;
using System;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Test.ProdutoAplicacaoTest;

public class DeletarProdutoTest : IClassFixture<ProdutoAplicacaoFixture>
{
    private readonly ProdutoAplicacaoFixture _fixture;

    public DeletarProdutoTest(ProdutoAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void DeletarAsync()
    {
        Produto produto = new Produto(_fixture.GuidProduto, "Nome do Produto", 12.34M);
        _fixture.produtoRepositorioMock.Setup(v => v.GetByIdAsync(_fixture.GuidProduto)).ReturnsAsync(produto);
        _fixture.produtoRepositorioMock.Setup(v => v.Delete(produto));
        _fixture.produtoRepositorioMock.Setup(v => v.CommitAsync());

        ProdutoAplicacao produtoAplicacao = _fixture.MakeSut();
        AtualizaProdutoModel modelo = new AtualizaProdutoModel();
        modelo.Guid = _fixture.GuidProduto;

        Task retorno = produtoAplicacao.DeletarAsync(Guid.NewGuid());
        Assert.Contains("Registro não encontrado.", retorno.Exception.Message);
    }
}
