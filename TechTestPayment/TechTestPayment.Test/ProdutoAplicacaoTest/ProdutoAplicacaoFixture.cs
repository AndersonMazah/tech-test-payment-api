﻿using AutoMapper;
using Moq;
using System;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Infra.Interfaces;

namespace TechTestPayment.Test.ProdutoAplicacaoTest;

public class ProdutoAplicacaoFixture
{
    public Mock<IProdutoRepositorio> produtoRepositorioMock { get; private set; }

    private IMapper _mapper;

    public Guid GuidProduto { get; set; }

    public ProdutoAplicacaoFixture()
    {
        produtoRepositorioMock = new Mock<IProdutoRepositorio>();

        if (_mapper is null)
        {
            MapperConfiguration config = new MapperConfiguration(config =>
            {
                config.CreateMap<ProdutoDto, Produto>().ReverseMap();
                config.CreateMap<ProdutoDto, Produto>().ReverseMap();
                config.CreateMap<VendaDto, Venda>().ReverseMap();
                config.CreateMap<ItemVendaDto, ItemVenda>().ReverseMap();
            });
            IMapper mapper = config.CreateMapper();
            _mapper = mapper;
        }
        GuidProduto = Guid.NewGuid();
    }

    public ProdutoAplicacao MakeSut()
    {
        return new ProdutoAplicacao(_mapper, produtoRepositorioMock.Object);
    }
}
