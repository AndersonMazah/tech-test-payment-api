﻿using Moq;
using System;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Dominio.Enumerators;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Test.VendaAplicacaoTest;

public class AtualizarVendaTest : IClassFixture<VendaAplicacaoFixture>
{
    private readonly VendaAplicacaoFixture _fixture;

    public AtualizarVendaTest(VendaAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void Atualizar()
    {
        Venda venda = new Venda(_fixture.GuidVenda, "1234567890", DateTime.Now, StatusVenda.AguardandoPagamento, 12.34M);
        _fixture.vendaRepositorioMock.Setup(v => v.GetByIdAsync(_fixture.GuidVenda)).ReturnsAsync(venda);
        _fixture.vendaRepositorioMock.Setup(v => v.Update(venda));
        _fixture.vendaRepositorioMock.Setup(v => v.CommitAsync());

        VendaAplicacao vendaAplicacao = _fixture.MakeSut();
        AtualizaVendaModel modelo = new AtualizaVendaModel();
        modelo.Guid = _fixture.GuidVenda;
        modelo.StatusVenda = StatusVenda.PagamentoAprovado;

        Task<VendaDto> retorno = vendaAplicacao.AtualizarAsync(modelo);
        Assert.NotNull(retorno.Result);
        Assert.Equal(_fixture.GuidVenda, retorno.Result.Guid);
    }
}
