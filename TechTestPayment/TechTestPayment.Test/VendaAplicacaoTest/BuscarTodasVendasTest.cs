﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Dominio.Enumerators;

namespace TechTestPayment.Test.VendaAplicacaoTest;

public class BuscarTodosVendaesTest : IClassFixture<VendaAplicacaoFixture>
{
    private readonly VendaAplicacaoFixture _fixture;

    public BuscarTodosVendaesTest(VendaAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void BuscarTodos()
    {
        Venda venda = new Venda(_fixture.GuidVenda, "1234567890", DateTime.Now, StatusVenda.AguardandoPagamento, 12.34M);
        _fixture.vendaRepositorioMock.Setup(v => v.GetAllAsync()).ReturnsAsync(new List<Venda>() { venda });

        VendaAplicacao vendaAplicacao = _fixture.MakeSut();

        Task<List<VendaDto>> retorno = vendaAplicacao.BuscarTodosAsync();
        Assert.NotNull(retorno.Result);
    }
}
