﻿using Moq;
using System;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Dominio.Enumerators;

namespace TechTestPayment.Test.VendaAplicacaoTest;

public class BuscarVendaPorGuidTest : IClassFixture<VendaAplicacaoFixture>
{
    private readonly VendaAplicacaoFixture _fixture;

    public BuscarVendaPorGuidTest(VendaAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void BuscarPorGuid()
    {
        Venda venda = new Venda(_fixture.GuidVenda, "1234567890", DateTime.Now, StatusVenda.AguardandoPagamento, 12.34M);
        _fixture.vendaRepositorioMock.Setup(v => v.GetByIdAsync(_fixture.GuidVenda)).ReturnsAsync(venda);

        VendaAplicacao vendaAplicacao = _fixture.MakeSut();

        Task<VendaDto> retorno = vendaAplicacao.BuscarPorGuidAsync(_fixture.GuidVenda);
        Assert.NotNull(retorno.Result);
        Assert.Equal(_fixture.GuidVenda, retorno.Result.Guid);
    }
}
