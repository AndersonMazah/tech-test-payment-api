﻿using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Dominio.Enumerators;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Test.VendaAplicacaoTest;

public class CadastrarVendaTest : IClassFixture<VendaAplicacaoFixture>
{
    private readonly VendaAplicacaoFixture _fixture;

    public CadastrarVendaTest(VendaAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void CadastrarSemItem()
    {
        string Codigo = "1234567890";
        Venda venda = new Venda(_fixture.GuidVenda, "1234567890", DateTime.Now, StatusVenda.AguardandoPagamento, 12.34M);
        _fixture.vendaRepositorioMock.Setup(v => v.AddAsync(venda));
        _fixture.vendaRepositorioMock.Setup(v => v.CommitAsync());

        VendaAplicacao vendaAplicacao = _fixture.MakeSut();
        CadastroVendaModel modelo = new CadastroVendaModel();
        modelo.Codigo = Codigo;

        Task<VendaDto> retorno = vendaAplicacao.CadastrarAsync(modelo);
        Assert.NotNull(retorno.Exception);
    }

    [Fact]
    public void CadastrarComItem()
    {
        string Codigo = "1234567890";
        Guid GuidProduto = Guid.NewGuid();
        Venda venda = new Venda(_fixture.GuidVenda, "1234567890", DateTime.Now, StatusVenda.AguardandoPagamento, 12.34M);
        _fixture.vendaRepositorioMock.Setup(v => v.AddAsync(venda));
        _fixture.vendaRepositorioMock.Setup(v => v.CommitAsync());
        Produto produto = new Produto(GuidProduto, "Nome do Produto", 12.34M);
        _fixture.produtoRepositorioMock.Setup(v => v.GetByIdAsync(GuidProduto)).ReturnsAsync(produto);

        VendaAplicacao vendaAplicacao = _fixture.MakeSut();
        CadastroVendaModel modelo = new CadastroVendaModel();
        modelo.Codigo = Codigo;

        ItemVendaModel item = new ItemVendaModel();
        item.ProdutoGuid = GuidProduto;
        item.Quantidade = 1;
        modelo.ItensVenda = new List<ItemVendaModel>();
        modelo.ItensVenda.Add(item);

        Task<VendaDto> retorno = vendaAplicacao.CadastrarAsync(modelo);
        Assert.NotNull(retorno.Result);
        Assert.True(retorno.Result.Codigo == Codigo);
    }

}
