﻿using Moq;
using System;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Dominio.Enumerators;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Test.VendaAplicacaoTest;

public class DeletarVendaTest : IClassFixture<VendaAplicacaoFixture>
{
    private readonly VendaAplicacaoFixture _fixture;

    public DeletarVendaTest(VendaAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void DeletarAsync()
    {
        Venda venda = new Venda(_fixture.GuidVenda, "1234567890", DateTime.Now, StatusVenda.AguardandoPagamento, 12.34M);
        _fixture.vendaRepositorioMock.Setup(v => v.GetByIdAsync(_fixture.GuidVenda)).ReturnsAsync(venda);
        _fixture.vendaRepositorioMock.Setup(v => v.Delete(venda));
        _fixture.vendaRepositorioMock.Setup(v => v.CommitAsync());

        VendaAplicacao vendaAplicacao = _fixture.MakeSut();
        AtualizaVendaModel modelo = new AtualizaVendaModel();
        modelo.Guid = _fixture.GuidVenda;

        Task retorno = vendaAplicacao.DeletarAsync(Guid.NewGuid());
        Assert.Contains("Registro não encontrado.", retorno.Exception.Message);
    }
}
