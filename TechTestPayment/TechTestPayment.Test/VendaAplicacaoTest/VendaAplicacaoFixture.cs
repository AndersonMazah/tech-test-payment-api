﻿using AutoMapper;
using Moq;
using System;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Infra.Interfaces;

namespace TechTestPayment.Test.VendaAplicacaoTest;

public class VendaAplicacaoFixture
{
    public Mock<IVendaRepositorio> vendaRepositorioMock { get; private set; }
    public Mock<IProdutoRepositorio> produtoRepositorioMock { get; private set; }

    private IMapper _mapper;

    public Guid GuidVenda { get; set; }

    public VendaAplicacaoFixture()
    {
        vendaRepositorioMock = new Mock<IVendaRepositorio>();
        produtoRepositorioMock = new Mock<IProdutoRepositorio>();

        if (_mapper is null)
        {
            MapperConfiguration config = new MapperConfiguration(config =>
            {
                config.CreateMap<VendaDto, Venda>().ReverseMap();
                config.CreateMap<ProdutoDto, Produto>().ReverseMap();
                config.CreateMap<VendaDto, Venda>().ReverseMap();
                config.CreateMap<ItemVendaDto, ItemVenda>().ReverseMap();
            });
            IMapper mapper = config.CreateMapper();
            _mapper = mapper;
        }
        GuidVenda = Guid.NewGuid();
    }

    public VendaAplicacao MakeSut()
    {
        return new VendaAplicacao(_mapper, vendaRepositorioMock.Object, produtoRepositorioMock.Object);
    }
}
