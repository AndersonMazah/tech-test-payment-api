﻿using Moq;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Test.VendedorAplicacaoTest;

public class AtualizarVendedorTest : IClassFixture<VendedorAplicacaoFixture>
{
    private readonly VendedorAplicacaoFixture _fixture;

    public AtualizarVendedorTest(VendedorAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void Atualizar()
    {
        Vendedor vendedor = new Vendedor(_fixture.GuidVendedor, "000.000.000-00", "Nome do Vendedor", "nome@email.com", "987654321");
        _fixture.vendedorRepositorioMock.Setup(v => v.GetByIdAsync(_fixture.GuidVendedor)).ReturnsAsync(vendedor);
        _fixture.vendedorRepositorioMock.Setup(v => v.Update(vendedor));
        _fixture.vendedorRepositorioMock.Setup(v => v.CommitAsync());

        VendedorAplicacao vendedorAplicacao = _fixture.MakeSut();
        AtualizaVendedorModel modelo = new AtualizaVendedorModel();
        modelo.Guid = _fixture.GuidVendedor;

        Task<VendedorDto> retorno = vendedorAplicacao.AtualizarAsync(modelo);
        Assert.NotNull(retorno.Result);
        Assert.Equal(_fixture.GuidVendedor, retorno.Result.Guid);
    }
}
