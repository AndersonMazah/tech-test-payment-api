﻿using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;

namespace TechTestPayment.Test.VendedorAplicacaoTest;

public class BuscarTodosVendedoresTest : IClassFixture<VendedorAplicacaoFixture>
{
    private readonly VendedorAplicacaoFixture _fixture;

    public BuscarTodosVendedoresTest(VendedorAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void BuscarTodos()
    {
        Vendedor vendedor = new Vendedor(_fixture.GuidVendedor, "000.000.000-00", "Nome do Vendedor", "nome@email.com", "987654321");
        _fixture.vendedorRepositorioMock.Setup(v => v.GetAllAsync()).ReturnsAsync(new List<Vendedor>() { vendedor });

        VendedorAplicacao vendedorAplicacao = _fixture.MakeSut();

        Task<List<VendedorDto>> retorno = vendedorAplicacao.BuscarTodosAsync();
        Assert.NotNull(retorno.Result);
        Assert.Single(retorno.Result);
        Assert.True(retorno.Result.Where(v => v.Guid == _fixture.GuidVendedor).Any());
    }
}
