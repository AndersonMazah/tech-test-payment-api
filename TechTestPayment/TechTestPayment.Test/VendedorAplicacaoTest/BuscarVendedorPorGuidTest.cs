﻿using Moq;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;

namespace TechTestPayment.Test.VendedorAplicacaoTest;

public class BuscarVendedorPorGuidTest : IClassFixture<VendedorAplicacaoFixture>
{
    private readonly VendedorAplicacaoFixture _fixture;

    public BuscarVendedorPorGuidTest(VendedorAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void BuscarPorGuid()
    {
        Vendedor vendedor = new Vendedor(_fixture.GuidVendedor, "000.000.000-00", "Nome do Vendedor", "nome@email.com", "987654321");
        _fixture.vendedorRepositorioMock.Setup(v => v.GetByIdAsync(_fixture.GuidVendedor)).ReturnsAsync(vendedor);

        VendedorAplicacao vendedorAplicacao = _fixture.MakeSut();

        Task<VendedorDto> retorno = vendedorAplicacao.BuscarPorGuidAsync(_fixture.GuidVendedor);
        Assert.NotNull(retorno.Result);
        Assert.Equal(_fixture.GuidVendedor, retorno.Result.Guid);
    }
}
