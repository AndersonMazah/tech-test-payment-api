﻿using System.Threading.Tasks;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Test.VendedorAplicacaoTest;

public class CadastrarVendedorTest : IClassFixture<VendedorAplicacaoFixture>
{
    private readonly VendedorAplicacaoFixture _fixture;

    public CadastrarVendedorTest(VendedorAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void Cadastrar()
    {
        string cpf = "000.000.000-00";
        Vendedor vendedor = new Vendedor(_fixture.GuidVendedor, cpf, "Nome do Vendedor", "nome@email.com", "987654321");
        _fixture.vendedorRepositorioMock.Setup(v => v.AddAsync(vendedor));
        _fixture.vendedorRepositorioMock.Setup(v => v.CommitAsync());

        VendedorAplicacao vendedorAplicacao = _fixture.MakeSut();
        CadastroVendedorModel modelo = new CadastroVendedorModel();
        modelo.Cpf = cpf;

        Task<VendedorDto> retorno = vendedorAplicacao.CadastrarAsync(modelo);
        Assert.NotNull(retorno.Result);
        Assert.True(retorno.Result.Cpf == cpf);
    }
}
