﻿using Moq;
using System;
using System.Threading.Tasks;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Dominio.Models;

namespace TechTestPayment.Test.VendedorAplicacaoTest;

public class DeletarVendedorTest : IClassFixture<VendedorAplicacaoFixture>
{
    private readonly VendedorAplicacaoFixture _fixture;

    public DeletarVendedorTest(VendedorAplicacaoFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void DeletarAsync()
    {
        Vendedor vendedor = new Vendedor(_fixture.GuidVendedor, "000.000.000-00", "Nome do Vendedor", "nome@email.com", "987654321");
        _fixture.vendedorRepositorioMock.Setup(v => v.GetByIdAsync(_fixture.GuidVendedor)).ReturnsAsync(vendedor);
        _fixture.vendedorRepositorioMock.Setup(v => v.Delete(vendedor));
        _fixture.vendedorRepositorioMock.Setup(v => v.CommitAsync());

        VendedorAplicacao vendedorAplicacao = _fixture.MakeSut();
        AtualizaVendedorModel modelo = new AtualizaVendedorModel();
        modelo.Guid = _fixture.GuidVendedor;

        Task retorno = vendedorAplicacao.DeletarAsync(Guid.NewGuid());
        Assert.Contains("Registro não encontrado.", retorno.Exception.Message);
    }
}
