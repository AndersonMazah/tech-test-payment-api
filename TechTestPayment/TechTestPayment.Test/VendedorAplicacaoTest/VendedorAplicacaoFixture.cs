﻿using AutoMapper;
using Moq;
using System;
using System.Security.Cryptography.X509Certificates;
using TechTestPayment.Aplicacao.DTOs;
using TechTestPayment.Aplicacao.Services;
using TechTestPayment.Dominio.Entities;
using TechTestPayment.Infra.Interfaces;

namespace TechTestPayment.Test.VendedorAplicacaoTest;

public class VendedorAplicacaoFixture
{
    public Mock<IVendedorRepositorio> vendedorRepositorioMock { get; private set; }

    private IMapper _mapper;

    public Guid GuidVendedor { get; set; }

    public VendedorAplicacaoFixture()
    {
        vendedorRepositorioMock = new Mock<IVendedorRepositorio>();

        if (_mapper is null)
        {
            MapperConfiguration config = new MapperConfiguration(config =>
            {
                config.CreateMap<VendedorDto, Vendedor>().ReverseMap();
                config.CreateMap<ProdutoDto, Produto>().ReverseMap();
                config.CreateMap<VendaDto, Venda>().ReverseMap();
                config.CreateMap<ItemVendaDto, ItemVenda>().ReverseMap();
            });
            IMapper mapper = config.CreateMapper();
            _mapper = mapper;
        }
        GuidVendedor = Guid.NewGuid();
    }

    public VendedorAplicacao MakeSut()
    {
        return new VendedorAplicacao(_mapper, vendedorRepositorioMock.Object);
    }
}
